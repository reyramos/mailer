<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<title>Claim Your Despicable Me Prize from Licensing Show</title>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="600" height="900" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_01.jpg" width="117" height="87" alt="">
		</td>
		<td width="371" height="87" style="vertical-align:bottom;height:87px;width:371px">
			<span style="font-family:Verdana,sans-serif;font-size:43px;color:#f37021">Congratulations!</span>
		<td>
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_03.jpg" width="112" height="87" alt="">
		</td>
		</td></tr>
	<tr>
		<td rowspan="4">
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_04.jpg" width="117" height="261" alt="">
		</td>
		<td width="371" height="167" style="text-align:center;height:167px;width:371px">
			<?php echo $name; ?>
		<td rowspan="4">
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_06.jpg" width="112" height="261" alt="">
		</td>
		</td></tr>
	<tr>
		<td>
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_07.jpg" width="371" height="27" alt="">
		</td>
	</tr>
	<tr>
		<td width="371" height="54" style="text-align:center;height:54px;width:371px">
			<span style="text-align:center;font-family:Verdana,sans-serif;font-size:17px;font-weight:bold;color:#5c6f7c;padding-left:0px;padding-right:0px">by playing Universal&#39;s Minions<br>Experience at Licensing Expo 2014.</span>
		</td>
	</tr>
	<tr>
		<td>
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_09.jpg" width="371" height="13" alt="">
		</td>
	</tr>
	<tr>
		<td>
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_10.jpg" width="117" height="293" alt="">
		</td>
		<td width="371" height="293" style="background-color:#f37021;width:371px;height:293px;text-align:left;padding:0px">
			<p style="font-weight:bold;font-size:11px;font-family:Verdana,sans-serif;text-align:left;color:#ffffff;line-height:17px">
				Reply to this email with the below details so we<br>can ship your prize to you! </p><span style="text-align:left;font-family:Verdana,sans-serif;font-size:17px;color:#ffffff">NAME<br>COMPANY NAME <span
			style="font-size:9px">(if shipping to your office)</span><br>STREET ADDRESS<br>CITY, STATE ZIP<br>COUNTRY<br>PHONE NUMBER <span style="font-size:9px">(to be used for delivery questions only)</span></span><br>

			<p style="font-weight:bold;font-size:11px;font-family:Verdana,sans-serif;text-align:center;line-height:17px;color:#ffffff">Please respond by Friday, June 27, 2014, or your<br>prize
				will be forfeited. Thanks for playing!</p>
		<td width="112" height="293" style="background-color:#f37021;width:112px;height:293px;text-align:left;padding:0px"></td>
		</td></tr>
	<tr>
		<td>
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_13.jpg" width="117" height="259" alt="">
		</td>
		<td>
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_14.jpg" width="371" height="259" alt="">
		</td>
		<td>
			<img style="display:inherit;" src="http://mm.creative.s3.amazonaws.com/UNIVERSAL_STUDIOS_LICENSING/_EMAIL_V2/universal_studios_licensing_email_v2_15.jpg" width="112" height="259" alt="">
		</td>
	</tr>
</table>

</body>
</html>