<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Welcome to myBreyer Guide</title>

	<style>
		*{ margin : 0; padding : 0 }

		*{ font-family : "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif }

		img{ max-width : 100% }

		body{ -webkit-font-smoothing : antialiased; -webkit-text-size-adjust : none; width : 100% !important; height : 100% }

		a{ color : #2ba6cb }

		.callout a{ font-weight : bold; color : #2ba6cb }

		table.social{ background-color : #ebebeb }

		table.head-wrap{ width : 100% }

		.header.container table td.logo{ padding : 15px }

		.header.container table td.label{ padding : 15px; padding-left : 0 }

		table.body-wrap{ width : 100% }

		table.footer-wrap{ width : 100%; clear : both !important }

		.footer-wrap .container td.content p{ border-top : 1px solid #d7d7d7; padding-top : 15px }

		.footer-wrap .container td.content p{ font-size : 10px; font-weight : bold }

		h1, h2, h3, h4, h5, h6{ font-family : "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height : 1.1; margin-bottom : 15px; color : #000 }

		h1 small, h2 small, h3 small, h4 small, h5 small, h6 small{ font-size : 60%; color : #6f6f6f; line-height : 0; text-transform : none }

		h1{ font-weight : 200; font-size : 44px }

		h2{ font-weight : 200; font-size : 37px }

		h3{ font-weight : 500; font-size : 27px }

		h4{ font-weight : 500; font-size : 23px }

		h5{ font-weight : 900; font-size : 17px }

		h6{ font-weight : 900; font-size : 14px; text-transform : uppercase; color : #444 }

		p, ul{ margin-bottom : 10px; font-weight : normal; font-size : 14px; line-height : 1.6 }

		p.lead{ font-size : 17px }

		ul li{ margin-left : 5px; list-style-position : inside }

		ul.sidebar li{ display : block; margin : 0 }

		ul.sidebar li a{ text-decoration : none; color : #666; padding : 10px 16px; margin-right : 10px; cursor : pointer; border-bottom : 1px solid #777; border-top : 1px solid #fff; display : block; margin : 0 }

		ul.sidebar li a.last{ border-bottom-width : 0 }

		ul.sidebar li a h1, ul.sidebar li a h2, ul.sidebar li a h3, ul.sidebar li a h4, ul.sidebar li a h5, ul.sidebar li a h6, ul.sidebar li a p{ margin-bottom : 0 !important }

		.container{ display : block !important; max-width : 600px !important; margin : 0 auto !important; clear : both !important }

		.content{ padding : 15px; max-width : 600px; margin : 0 auto; display : block }

		.content table{ width : 100% }

		.column tr td{ padding : 15px }

		.column table{ width : 100% }

		.clear{ display : block; clear : both }

		.text-red{
			color : #ed3e49;
		}

		.text-success{
			color : #468847;
		}
	</style>
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

</head>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- HEADER -->
<table style="width: 100%" width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="head-wrap" background="https://s3.amazonaws.com/mybreyerguide.com/site/border.png">
	<tr>
		<td></td>
		<td class="header container">

			<div class="content" style="padding: 0px">
				<table>
					<tr>
						<img src="https://s3.amazonaws.com/mybreyerguide.com/site/logo.png" alt="myBreyerGuide" height="60"/>
					</tr>
				</table>
			</div>

		</td>
		<td></td>
	</tr>
</table>
<!-- /HEADER -->


<!-- BODY -->
<table style="width: 100%" width="100%" border="0" cellpadding="0" cellspacing="0" class="body-wrap">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">

			<div class="content">
				<table style="width: 100%" width="100%" border="0" cellpadding="0" cellspacing="0" class="" align="center">
					<tr>
						<td>

							<h3>Welcome, <?php echo $fname . " " . $lname; ?></h3>

							<p class="lead"></p>

							<!-- A Real Hero (and a real human being) -->
							<p><img src="https://s3.amazonaws.com/mybreyerguide.com/site/welcome.jpg" alt="welcome_img"/></p><!-- /hero -->

							<h3>Hello, welcome to myBreyer Guide</h3>

							<p data-ng-bind-html="post.content" class="ng-binding">

							<p>My name is Rey Ramos, the site administrator and developer, and I wanted to personally thank you for visiting myBreyer Guide. I hope that you like what we are creating
								and hope you will join our community.

							</p>

							<p>The idea for myBreyer Guide has been in the works for a few years now and my wife Nicole (AKA Dot) Ramos wanted access to an application where she can keep her models'
								information, awards and records in a place where she can easily access anywhere.</p>

							<p>For the past few years I have attended many model horse shows, and I've learned to appreciate the amount of work that the model horse community spends on their models.
								There are lots of beautiful models and I did not see anyone keeping a more modern way of tracking their awards and documentations.</p>

							<p>My goal for this site is to provide a cloud-centric storage of your models, where you can be at a model horse show on your mobile smartphone or tablet and update each
								model's status, while also sharing it on social media. myBreyer Guide is currently in Beta, which means I will keep adding new features, while we work out all the bugs
								with the help of the model horse show community volunteer testers. The site as a whole will be available for use at no charge, however there may be some advanced
								features available at a modest fee in order to help maintain the site in it's growth..</p>

							<p>All I'm asking is that you please be patient with me while I work on bringing you a great experience. I am accepting ideas and features enhancements, so feel free to
								contact me with any ideas you may have. There are no bad ideas and you may email me at <a href="mailto:reyramos@myphpdeligths.com?subject=Welcome to myBreyer Guide">reyramos@myphpdeligths.com</a>.
							</p>

							<p></p>
							<ol>
								<li><strong>Some of the feature that are in working progress and other that are coming.</strong>
									<ul class="list-unstyled">
										<li class="text-success" style="color: #468847;"><i class="fa fa-check"></i> Account Sign In</li>
										<li class=""><i class="fa "></i> Show Recent model added</li>
									</ul>
									<p></p></li>
								<li><strong>Features while logged in</strong>
									<ul class="list-unstyled">
										<li class=""><i class="fa "></i>Like a Model</li>
										<li class=""><i class="fa "></i>Search a model and add to your List</li>
										<li class=""><i class="fa "></i>Share on FB</li>
										<li class=""><i class="fa "></i>Add Model</li>
										<li class=""><i class="fa "></i>Update Model</li>
										<li class=""><i class="fa "></i>Forum Post</li>
									</ul>
									<p></p></li>
							</ol>
							<p></p></p>
							<br/>
							<br/>

							<!-- social & contact -->
							<table class="social" width="100%" align="center" style="background-color: #ebebeb;" cellpadding="10">
								<tr>
									<td>


										<!--- column 2 -->
										<table align="left" cellpadding="10">
											<tr>
												<td>
													<h4><i class="fa fa-map-marker text-red" style="color : #ed3e49;"></i> Contact Us</h4>

													<p>Do not hesitate to contact us if you have any questions or feature requests:</p>

													<p>
														Lake Mary, FL 32746<br/>
														Greenwood Lakes<br/>
														Phone: +1 (321)331-9096<br/>
														Email: <a href="mailto:reyramos@myphpdelights.com">reyramos@myphpdelights.com</a>
													</p>

												</td>
											</tr>
										</table>
										<!-- /column 2 -->

										<span class="clear"></span>

									</td>
								</tr>
							</table>
							<!-- /social & contact -->
						</td>
					</tr>
				</table>
			</div>

		</td>
		<td></td>
	</tr>
</table>
<!-- /BODY -->

<!-- FOOTER -->
<table style="width: 100%" width="100%" border="0" cellpadding="0" cellspacing="0" class="footer-wrap">
	<tr>
		<td></td>
		<td class="container">
			<!-- content -->
			<div class="content">
				<table style="width: 100%" width="100%" border="0" cellpadding="0" cellspacing="0" class="" align="center">
					<tr>
						<td align="center">
							<p>
								<a href="#">Terms</a> |
								<a href="#">Privacy</a> |
								<a href="#">
									<unsubscribe>Unsubscribe</unsubscribe>
								</a>
							</p>
						</td>
					</tr>
				</table>
			</div>
			<!-- /content -->
		</td>
		<td></td>
	</tr>
</table>
<!-- /FOOTER -->

</body>
</html>