<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Welcome to myBreyer Guide</title>
</head>
<body class="fixed-header">

<header id="header">
	<div class="topic">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<h3>myBreyer Guide API</h3>
				</div>
			</div>
		</div>
	</div>
</header>
<div id="main">
	<div class="container">
		<div class="row">
			<div class="col-sm-10">
				<div data-ng-view></div>
				<div class="clearfix"></div>
			</div>
			<div class="col-sm-2">
				<h3 class="headline"><span>Resources</span></h3>
				<ul class="list-style-none resources">
					<li><a href="/">Introduction</a></li>
					<li><a href="#/user">User</a></li>
					<li><a href="#/register">Register</a></li>
					<li><a href="#/models">Models</a></li>
					<li><a href="#/forums">Forums</a></li>
				</ul>
			</div>
		</div> <!-- / .row -->
	</div>

	<!-- Footer -->
	<footer>
		<div class="container">
			<div class="row">
				<!-- Contact Us -->
				<div class="col-sm-4">
					<h4><i class="fa fa-map-marker text-red"></i> Contact Us</h4>
					<p>Do not hesitate to contact us if you have any questions or feature requests:</p>
					<p>
						Lake Mary, FL 32746<br />
						Greenwood Lakes<br />
						Phone: +1 (321)331-9096<br />
						Email: <a href="mailto:reyramos@myphpdelights.com">reyramos@myphpdelights.com</a>
					</p>
				</div>
			</div>
		</div>
	</footer>


	<!-- Copyright -->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="copyright">
					Copyright 2014 - myBreyer Guide by <a href="https://myphpdelights.com">myPHP Delights LLC</a> | All Rights Reserved
				</div>
			</div>
		</div>  <!-- / .row -->
	</div> <!-- / .container -->
</div>
<!-- /FOOTER -->

</body>
</html>