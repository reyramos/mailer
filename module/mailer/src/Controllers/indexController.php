<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 6/10/14
 * Time: 4:59 PM
 */

namespace mailer\Controllers;


use mailer\Enums\MsgCodes;

class indexController
{
	protected static $jsonRequest;
	protected $from = "noReply@mybreyerguide.com";
	protected $results;

	protected static $config;

	public static function configure($configuration)
	{

		if (file_exists($configuration)) {
			self::$config = include_once $configuration;
		} else {
			//some error
			self::setResults(MsgCodes::FAILED);
			self::printResult(false);
		}

	}

	public static function getRequest()
	{
		//THIS HAS TO BE AVAILABLE FOR EVERY REQUEST
		return self::$jsonRequest = json_decode(stream_get_contents(fopen('php://input', 'r')), true);
	}

	public function setFrom($email)
	{
		$this->from = $email;
	}

	private function ensureIndex($request, $array)
	{

		if (array_key_exists($request, self::$config)) {
			$handler = array();
			foreach (self::$config[$request]['ensureIndex'] as $key => $var) {
				if ($var) {
					if (array_key_exists($key, $array)) {
						$handler = array_merge($handler, array($key => $array[$key]));
					} else {
						self::setResults(MsgCodes::EMAIL_MISSING_PARAMS);
						return false;
					}
				}
			}

			return true;
		} else {
			self::setResults(MsgCodes::BAD_REQUEST);
		}

		return false;

	}

	public function devSendMail($template, $subject){
		ob_start();
		$array = json_decode('{"email":"reymundo.ramos@gmail.com", "fname":"Rey","lname":"Ramos"}', true);

		$result = $this->ensureIndex($template, $array);
		if ($result):

			$array['to'] = $array['email'];

			$handler = array();
			foreach ($array as $key => $var) {
				$handler = array_merge($handler, array($key => $var));
			}

			\Flight::render(unserialize(FILE_TEMPLATES)[$template], $handler);

			$content = ob_get_clean();
			ob_end_clean();

			$transport = \Swift_SmtpTransport::newInstance('localhost',25);

			//development transport
			if ($_SERVER['APPLICATION_ENV'] == 'development') {
				$transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
					->setUsername('reymundo.ramos@gmail.com')
					->setPassword('tycnbcorfaofifda');
			}

			// Create the Mailer using your created Transport
			$mailer = \Swift_Mailer::newInstance($transport);

			// Create a message
			$message = \Swift_Message::newInstance($subject);

			$message->setFrom(array($this->from => 'myBreyer Guide'))
				->setReplyTo(array($this->from => 'myBreyer Guide'))
				->setTo(array($array['to']))
				->setContentType("text/plain; charset=UTF-8")
				->setBody("If you cannot view this message, Sorry, your email client might not support HTML content", 'text/plain')
				->addPart($content, 'text/html')
				->setReturnPath($this->from);

			$message->getHeaders()->get('Message-ID')->setId(sprintf('%s@mybreyerguide.com', uniqid(microtime(true), true)));

			// Send the message
			$result = $mailer->send($message);
			$code = $result ? MsgCodes::EMAIL_SUCCESS : MsgCodes::EMAIL_FAILED;
			self::setResults($code);

		endif;


		self::printResult($result);

	}

	public function sendMail($template, $subject)
	{
		ob_start();
		$array = self::$jsonRequest;
		$result = $this->ensureIndex($template, $array);

		if ($result):

			$array['to'] = $array['email'];

			$handler = array();
			foreach ($array as $key => $var) {
				$handler = array_merge($handler, array($key => $var));
			}

			\Flight::render(unserialize(FILE_TEMPLATES)[$template], $handler);

			$content = ob_get_clean();
			ob_end_clean();

			$transport = \Swift_SmtpTransport::newInstance('localhost',25);

			//development transport
			if ($_SERVER['APPLICATION_ENV'] == 'development') {
				$transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
					->setUsername('reymundo.ramos@gmail.com')
					->setPassword('tycnbcorfaofifda');
			}

			// Create the Mailer using your created Transport
			$mailer = \Swift_Mailer::newInstance($transport);

			// Create a message
			$message = \Swift_Message::newInstance($subject);

			$message->setFrom(array($this->from => 'myBreyer Guide'))
				->setReplyTo(array($this->from => 'myBreyer Guide'))
				->setTo(array($array['to']))
				->setContentType("text/plain; charset=UTF-8")
				->setBody("If you cannot view this message, Sorry, your email client might not support HTML content", 'text/plain')
				->addPart($content, 'text/html')
				->setReturnPath($this->from);

//			$message->getHeaders()->get('Message-ID')->setId(sprintf('%s@mybreyerguide.com', uniqid(microtime(true), true)));

			// Send the message
			$result = $mailer->send($message);
			$code = $result ? MsgCodes::EMAIL_SUCCESS : MsgCodes::EMAIL_FAILED;
			self::setResults($code);

		endif;


			self::printResult($result);


	}


	protected function setResults($code)
	{
		$this->results['data'] = array(
			"code" => $code
		, "message" => MsgCodes::msg_toString($code)
		);
	}

	protected function printResult($bool)
	{
		$this->results['STATUS'] = $bool ? "OK" : "FAILED";
		echo json_encode($this->results);
	}

}