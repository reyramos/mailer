<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 6/11/14
 * Time: 5:25 PM
 */

namespace mailer\Enums;


final class MsgCodes extends \SplEnum
{

	const __default = self::EMAIL_SUCCESS;

	const EMAIL_SUCCESS = 10;
	const EMAIL_FAILED = 20;
	const EMAIL_ADDRESS_EXIST = 30;
	const EMAIL_MISSING_PARAMS = 40;
	const FAILED = 61;
	const USER_AUTH_EXPIRED = 100;
	const NOT_ENOUGH_CHARS = 101;
	const DUPLICATE = 103;
	const SUCCESS = 200;
	const FOUND = 303;
	const REDIRECT = 303;
	const BAD_REQUEST = 400;
	const FORBIDDEN = 403;
	const SERVER_ERROR = 500;

	public static function enum($value, $options, $default = '')
	{

		if ($value !== null) {
			if (array_key_exists($value, $options)) {
				return $options[$value];
			}
			return $options[$default];
		}
		return "NOT_SET";

	}

	public static function msg_toString($value = null)
	{
		$options = array(
			self::EMAIL_SUCCESS => "EMAIL_SUCCESS",
			self::EMAIL_ADDRESS_EXIST => "EMAIL_ADDRESS_EXIST",
			self::FAILED => "FAILED",
			self::USER_AUTH_EXPIRED => "USER_AUTH_EXPIRED",
			self::NOT_ENOUGH_CHARS => "NOT_ENOUGH_CHARS",
			self::DUPLICATE => "DUPLICATE",
			self::SUCCESS => "SUCCESS",
			self::FOUND => "FOUND",
			self::REDIRECT => "REDIRECT",
			self::BAD_REQUEST => "BAD_REQUEST",
			self::FORBIDDEN => "FORBIDDEN",
			self::FORBIDDEN => "FORBIDDEN",
			self::SERVER_ERROR => "SERVER_ERROR",
			self::EMAIL_MISSING_PARAMS => "EMAIL_MISSING_PARAMS",
		);

		return self::enum($value, $options, self::EMAIL_SUCCESS);
	}

}