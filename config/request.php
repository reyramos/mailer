<?php

return array(
	'welcome' => array(
		"ensureIndex" => array("email" => 1, "fname" => 1, "lname" => 1)
	),
	'sample' => array(
		"ensureIndex" => array("to" => 1, "name" => 1)
	)
);
