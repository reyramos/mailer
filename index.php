<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 6/10/14
 * Time: 2:25 PM
 */

if ($_SERVER['APPLICATION_ENV'] == 'development') {
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}


require_once __DIR__ . '/vendor/autoload.php';

use mailer\Controllers;


define('__API__', __DIR__);
define('__PUBLIC__', __DIR__ . "/public");
define('__URI__', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);

Controllers\indexController::getRequest();
Controllers\indexController::configure(__DIR__ . '/config/request.php');

$FILE_TEMPLATES = array(
	"index" => "/index.php"
, "sample" => "/sample-email.php"
, "registration" => "/registration_email_success.php"
, "welcome" => "/welcome_email.php"
, "contactMe" => "/contact_me.php"
);

define('FILE_TEMPLATES', serialize($FILE_TEMPLATES));


/**
 * Index Page
 * Get Request from POST[Content-type: application/json]
 */


/**
 * Make changes to custom \Flight framework
 */
//set the view path for Flight
Flight::set('flight.views.path', __PUBLIC__);

/**
 * Start of RESTful services
 */
Flight::route('/', function () {
	echo "Welcome Mailer";
});

Flight::route('POST /welcome', function () {
	$cont = new Controllers\indexController();
	$cont->sendMail('welcome', 'Welcome to myBreyer Guide');
	die();
});

Flight::route('POST /contact-me', function () {
	$cont = new Controllers\indexController();
	$cont->sendMail('contactMe', 'Message to myBreyer Guide');
	die();
});

Flight::route('POST /registration', function () {
	$cont = new Controllers\indexController();
	$cont->sendMail('registration', 'Registration to myBreyer Guide');
	die();
});

Flight::route('/developer_email_test', function () {
	$cont = new Controllers\indexController();
	$cont->devSendMail('welcome', 'Welcome to myBreyer Guide');
	die();
});

Flight::start();